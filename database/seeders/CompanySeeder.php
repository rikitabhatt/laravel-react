<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company')->insert([
            'company_name' => 'Consumer Sketch',
            'company_url' => 'https://www.consumer-sketch.com',
            'company_phone' =>'+91-265-2988888',
            'company_email' => 'info@consumer-sketch.com',
            'open_time' =>'9:00',
        ]);
    }
}
