<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(ModulesSeeder::class);
        $this->call(Module_actionsSeeder::class);
        $this->call(PermissionsSeeders::class);
        $this->call(UserrolesSeeders::class);
        $this->call(DesignationsSeeder::class);
    }
}
