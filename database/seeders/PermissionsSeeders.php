<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert(
            [
                'module_id' => 1,
                'module_action_id' => 1,
                'created_by'=> 1,
                'title'=>'create-project',
            ]
        );
    }
}
