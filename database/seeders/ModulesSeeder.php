<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'module_name' => 'sprint',
                'module_label' => 'sprint',
                'created_by'=> 1,
            ],
            [
                'module_name' => 'project',
                'module_label' => 'project',
                'created_by'=> 1,
            ],
            [
                'module_name' => 'task',
                'module_label' => 'task',
                'created_by'=> 1,
            ],
            [
                'module_name' => 'comment',
                'module_label' => 'comment',
                'created_by'=> 1,
            ],
            [
                'module_name' => 'worklog',
                'module_label' => 'worklog',
                'created_by'=> 1,
            ]]
        );
    }
}
