<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DesignationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('designations')->insert([
                [
                'designation_name' => 'developer',
                'designation_label' => 'developer'
                ],
                [
                'designation_name' => 'qa',
                'designation_label' => 'qa'
                ],
                [
                'designation_name' => 'qa',
                'designation_label' => 'qa'
                ],
                [
                'designation_name' => 'front-end',
                'designation_label' => 'front-end'
                ],
                [
                'designation_name' => 'designer',
                'designation_label' => 'designer'
                ],
                [
                'designation_name' => 'projectlead',
                'designation_label' => 'projectlead'
                ],
                [
                'designation_name' => 'projectmanager',
                'designation_label' => 'projectmanager'
                ],
                [
                'designation_name' => 'bd',
                'designation_label' => 'bd'
                ],
                [
                'designation_name' => 'sco',
                'designation_label' => 'sco'
                ],
            ]);
    }
}
