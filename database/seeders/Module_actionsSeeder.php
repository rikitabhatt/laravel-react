<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Module_actionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module_actions')->insert([
            [
                'action_name' => 'create',
                'action_label' => 'create',
                'created_by'=> 1,
            ],
            [
                'action_name' => 'edit',
                'action_label' => 'edit',
                'created_by'=> 1,
            ],
            [
                'action_name' => 'delete',
                'action_label' => 'task',
                'created_by'=> 1,
            ],
            [
                'action_name' => 'close',
                'action_label' => 'close',
                'created_by'=> 1,
            ],
            [
                'action_name' => 'browse',
                'action_label' => 'browse',
                'created_by'=> 1,
            ]]
        );
    }
}
