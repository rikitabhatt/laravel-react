<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('company_name', 255)->nullable();
            $table->string('company_url',200)->nullable();
            $table->string('company_phone',100)->nullable();
            $table->string('company_email')->unique();
            $table->time('open_time');
            $table->enum('monday',array('open', 'close'))->default('close');
            $table->enum('tuesday',array('open', 'close'))->default('close');
            $table->enum('wednesday',array('open', 'close'))->default('close');
            $table->enum('thursday',array('open', 'close'))->default('close');
            $table->enum('friday',array('open', 'close'))->default('close');
            $table->enum('saturday',array('open', 'close'))->default('close');
            $table->enum('sunday',array('open', 'close'))->default('close');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
