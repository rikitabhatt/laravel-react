<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('project_categories', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('name',100)->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_categories');
    }
}
