<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->Increments('id');
            $table->integer('project_id')->unsigned()->index()->nullable();
            $table->integer('reporter_id')->unsigned()->index()->nullable();
            $table->integer('assignee_id')->unsigned()->index()->nullable();
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->enum('issue_type', ['bug','story','epik','spike','task','sub-task'])->default('task');
            $table->string('issue_title')->nullable();
            $table->longText('description')->nullable();
            $table->integer('priority');
            $table->boolean('is_done')->comment('A value of zero is considered false. Nonzero values are considered true')->default(1);	
            $table->enum('status', ['planning_todo','todo','in-progress','redy_for_qa','qa_in_progress','back_to_dev','done','blocked','open_question','not_required']);
            $table->date('due_date')->nullable();
            $table->date('completed_date')->nullable();
            $table->string('original_estimate')->nullable();
            $table->string('estimate')->nullable();
            $table->string('spent')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('reporter_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('assignee_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
