<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\ModuleActionController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RolePermissionController;
use App\Http\Controllers\UserRoleController;
use App\Http\Controllers\PassportController;
use App\Http\Controllers\ProjectCategoryController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\DesignationController;
use App\Http\Controllers\ProjectUserAssignController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TaskCommentController;

Route::post('/register',[PassportController::class,'register']); 
Route::post('/login',[PassportController::class,'login']);
Route::get('/logout', [PassportController::class, 'logout'])->name('logout.user');
Route::get('/login',[PassportController::class,'login'])->name('login');

Route::middleware('auth:api')->get('/user', function (Request $request) {
   // return $request->user();
});
Route::get('/companylist', 'App\Http\Controllers\CompanyController@index');
Route::apiResource('company', CompanyController::class);
Route::POST('/companies','App\Http\Controllers\CompanyController@getAllList');

Route::apiResource('user', UserController::class);
Route::POST('/users','App\Http\Controllers\UserController@getAllList');
Route::POST('/user/getAllRecord','App\Http\Controllers\UserController@getAllRecord');

Route::apiResource('role',RoleController::class);
Route::POST('/roles','App\Http\Controllers\RoleController@getAllList');
Route::POST('/roles/getAllRecord','App\Http\Controllers\RoleController@getAllRecord');

Route::apiResource('module',ModuleController::class);
Route::POST('/modules','App\Http\Controllers\ModuleController@getAllList');
Route::POST('/module/getAllRecord','App\Http\Controllers\ModuleController@getAllRecord');

Route::apiResource('module_action',ModuleActionController::class);
Route::POST('/moduleactions','App\Http\Controllers\ModuleActionController@getAllList');
Route::POST('/module_action/getAllRecord','App\Http\Controllers\ModuleActionController@getAllRecord');

Route::apiResource('permission',PermissionController::class);
Route::POST('/permissions','App\Http\Controllers\PermissionController@getAllList');
Route::POST('/permission/getAllRecord','App\Http\Controllers\PermissionController@getAllRecord');

Route::apiResource('role_permission',RolePermissionController::class);
Route::POST('/role_permissions','App\Http\Controllers\RolePermissionController@getAllList');
Route::POST('/role_permission/getAllRecord','App\Http\Controllers\RolePermissionController@getAllRecord');

Route::apiResource('user_role',UserRoleController::class);
Route::POST('/user_roles','App\Http\Controllers\UserRoleController@getAllList');
Route::POST('/user_role/getAllRecord','App\Http\Controllers\UserRoleController@getAllRecord');

Route::apiResource('project',ProjectController::class);
Route::POST('/projects','App\Http\Controllers\ProjectController@getAllList');
Route::POST('/project/getAllRecord','App\Http\Controllers\ProjectController@getAllRecord');

Route::apiResource('project_user_assign',ProjectUserAssignController::class);
Route::POST('/project_user_assigns/{id}','App\Http\Controllers\ProjectUserAssignController@getAllList');
Route::POST('/project_user_assign/getAllRecord','App\Http\Controllers\ProjectUserAssignController@getAllRecord');

Route::apiResource('project_category',ProjectCategoryController::class);
Route::POST('/project_categories','App\Http\Controllers\ProjectCategoryController@getAllList');
Route::POST('/project_category/getAllRecord','App\Http\Controllers\ProjectCategoryController@getAllRecord');

Route::POST('designation/getAllRecord','App\Http\Controllers\DesignationController@getAllRecord');

Route::apiResource('task',TaskController::class);
Route::POST('/tasks/{filterby}','App\Http\Controllers\TaskController@getAllList');
Route::POST('/task/updateTitle','App\Http\Controllers\TaskController@updateTaskTitle');
Route::POST('/task/updateTaskAssignee','App\Http\Controllers\TaskController@updateTaskAssignee');
Route::POST('/task/updateTaskStatus','App\Http\Controllers\TaskController@updateTaskStatus');
Route::GET('/tasks/getAlltaskType','App\Http\Controllers\TaskController@getAlltaskType');
Route::GET('/tasks/getTaskBySlug/{task_slug}','App\Http\Controllers\TaskController@getTaskBySlug');

Route::apiResource('task_comment',TaskCommentController::class);
Route::POST('/task_comments/{TaskId}','App\Http\Controllers\TaskCommentController@getAllList');

// To Verify from Middle Ware
//->middleware('auth:api');