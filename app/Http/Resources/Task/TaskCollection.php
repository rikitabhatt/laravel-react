<?php
namespace App\Http\Resources\Task;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Project\ProjectCollection;
use Carbon\Carbon;
use App\Helpers\Helper;
class TaskCollection extends JsonResource
{   
    public function toArray($request)
    {   $project_details = $this->projects;
        $reporter_details = $this->reporters;
        $creators_details = $this->creators;
        $assignees_details = $this->assignees;
    
        return [
            'id' => $this->id,
            'task_slug'=>$this->task_slug,
            'project_id' => $this->project_id,
            'project_details'=> $project_details,
            'issue_title' =>$this->issue_title,
            'reporter_id'=> $reporter_details,
            'reporter_details'=> $reporter_details,
            'assignee_id'=> $this->assignee_id,
            'assignee_details'=> $assignees_details,
            'creator_id'=> $this->creator_id,
            'creators_details'=>$creators_details,
            'issue_type'=>$this->issue_type,
            'issue_title'=>$this->issue_title,
            'description'=> $this->description,
            'priority'=> $this->priority,
            'priority_name'=> $this->getPriorityName($this->priority),
            'is_done'=> $this->is_done,
            'status'=> $this->status,
            'statusDisplay'=> ucfirst(str_replace('_',' ',str_replace('-',' ',$this->status))),
            'due_date'=> $this->due_date,
            'completed_date'=>$this->completed_date,
            'original_estimate'=> $this->original_estimate,
            'estimate'=> $this->estimate,
            'spent'=> $this->spent,
            'created_at'=> Helper::relative_date(strtotime($this->created_at)),
            'updated_at'=> Helper::relative_date(strtotime($this->updated_at)),
            'deleted_at'=> Helper::relative_date(strtotime($this->deleted_at)),
        ];
    }
}
