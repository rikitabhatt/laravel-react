<?php
namespace App\Http\Resources\Module;
use Illuminate\Http\Resources\Json\JsonResource;

class ModuleCollection extends JsonResource
{
    public function toArray($request)
    { 
        return [
            'module_id' => $this->id,
            'module_name' => $this->module_name,
            'module_label' =>$this->module_label
        ];
    }
}
