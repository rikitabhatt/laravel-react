<?php
namespace App\Http\Resources\RolePermission;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Role\RoleCollection;
use App\Http\Resources\Permission\PermissionCollection;
class RolePermissionCollection extends JsonResource
{
    public function toArray($request) 
    {   $role_details = new RoleCollection($this->roles);
        $permission_details = new PermissionCollection($this->permissions);
        return [
            'role_permission_id'=> $this->id,
            'role_id'=>$this->role_id,
            'permission_id'=> $this->permission_id,
            'role_name'=> $role_details['role_name'],
            'permission_title'=> $permission_details['title'],
        ];
    }
}
