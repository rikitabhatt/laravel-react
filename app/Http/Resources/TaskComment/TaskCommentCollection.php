<?php
namespace App\Http\Resources\TaskComment;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskCommentCollection extends JsonResource
{   
    
    public function toArray($request)
    {   $createby_details = $this->users;
        return [
            'id' => $this->id,
            'task_id' => $this->task_id,
            'comment' =>$this->comment,
            'created_by' =>$this->created_by,
            'createby_details' => $createby_details,
            'comment_time' => date('d/M/y h:m A',strtotime($this->created_at))
        ];
    }
}
