<?php
namespace App\Http\Resources\ProjectCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectCategoryCollection extends JsonResource
{
    public function toArray($request)
    {  
        return [
            'project_cat_id' => $this->id,
            'project_cat_name' => $this->name,
            'project_cat_description' => $this->description
        ];
    }
}
