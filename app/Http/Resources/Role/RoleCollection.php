<?php
namespace App\Http\Resources\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class RoleCollection extends JsonResource
{
    public function toArray($request)
    { 
        return [
            'role_id' => $this->id,
            'role_name' => $this->role_name,
            'role_label' =>$this->role_label
        ];
    }
}
