<?php
namespace App\Http\Resources\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCollection extends JsonResource
{
    public function toArray($request)
    { 
        return [
            'user_id' => $this->id,
            'user_name' => $this->name,
            'user_email' =>$this->email,

        ];
    }
}
