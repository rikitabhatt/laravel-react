<?php
namespace App\Http\Resources\ModuleAction;
use Illuminate\Http\Resources\Json\JsonResource;

class ModuleActionCollection extends JsonResource
{
    public function toArray($request)
    {  
        return [
            'action_id' => $this->id,
            'action_name' => $this->action_name,
            'action_label' =>$this->action_label
        ];
    }
}
