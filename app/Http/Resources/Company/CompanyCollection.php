<?php
namespace App\Http\Resources\Company;
use Illuminate\Http\Resources\Json\JsonResource;

class CompanyCollection extends JsonResource
{
    public function toArray($request)
    { 
        return [
            'company_id' => $this->id,
            'company_name' => $this->company_name,
            'company_url' =>$this->company_url,
            'company_phone' => $this->company_phone,
            'company_email' =>$this->company_email, 
            'open_time'=>$this->open_time,
            'monday'=>$this->monday,
            'tuesday'=>$this->tuesday,
            'wednesday'=>$this->wednesday,
            'thursday'=>$this->thursday,
            'friday' =>$this->friday,
            'saturday' =>$this->saturday,
            'sunday'=>$this->sunday

        ];
    }
}
