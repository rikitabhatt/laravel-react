<?php
namespace App\Http\Resources\ProjectUserAssign;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Project\Projects;
class ProjectUserAssignCollection extends JsonResource
{
    public function toArray($request)
    {   
        $project_details = $this->projects;
        $designations_details = $this->designations;
        $users = $this->users;
        return [
            'project_user_asssign_id' => $this->id,
            'user_name' => $users->name,
            'user_id' => $this->user_id,
            'project_id' => $this->project_id,
            'designation_name' =>$designations_details->designation_name,
            'project_name' => $project_details->project_name.' ['.$project_details->project_key.']'
        ];
    }
}
