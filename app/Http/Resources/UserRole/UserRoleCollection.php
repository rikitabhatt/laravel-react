<?php
namespace App\Http\Resources\UserRole;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Permission\PermissionCollection;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\Role\RoleCollection;
class UserRoleCollection extends JsonResource
{
    public function toArray($request)
    { 
        $user_details = new UserCollection($this->users);
        $role_details = new RoleCollection($this->roles);
        return [
            'user_role_id' => $this->id,
            'role_id'=> $this->role_id,
            'user_id'=> $this->user_id,
            'role_name' => $role_details['role_name'],
            'user_name' =>$user_details['name']
        ];
    }
}
