<?php
namespace App\Http\Resources\Permission;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Module\ModuleCollection;
use App\Http\Resources\ModuleAction\ModuleActionCollection;
class PermissionCollection extends JsonResource
{
    public function toArray($request) 
    {   $module_details = new ModuleCollection($this->modules);
        $action_details = new ModuleActionCollection($this->moduleactions);
        return [
            'permission_id'=> $this->id,
            'title' => $this->title,
            'module_id'=>$module_details->id,
            'module_name'=> $module_details->module_name,
            'module_action_id'=> $action_details->id,
            'module_action_name'=> $action_details->action_name,
            
        ];
    }
}
