<?php
namespace App\Http\Resources\Project;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProjectCategory\ProjectCategoryCollection;

class ProjectCollection extends JsonResource
{
    public function toArray($request)
    {   $cat_details = new ProjectCategoryCollection($this->project_categories);
        return [
            'project_id'=>$this->id,
            'project_name' => $this->project_name,
            'project_key' => $this->project_key,
            'project_description' => $this->project_description,
            'project_category_id'=>$this->project_category_id,
            'project_category_name'=>$cat_details['name']
        ];
    }
}
