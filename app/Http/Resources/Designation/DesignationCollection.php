<?php
namespace App\Http\Resources\Designation;
use Illuminate\Http\Resources\Json\JsonResource;

class DesignationCollection extends JsonResource
{
    public function toArray($request)
    { 
        return [
            'designation_id' => $this->id,
            'designation_name' => $this->designation_name,
            'designation_label' =>$this->designation_label,
            'email' => $this->email
        ];
    }
}
