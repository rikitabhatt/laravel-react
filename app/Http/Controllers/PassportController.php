<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Task;
use Exception;
use Validator;
use Illuminate\Support\Facades\Hash;
class PassportController extends Controller
{
    public function register(Request $request)
    { 
        $input= $request->json()->all();
        $validator = Validator::make($request->all(),[
            'user_name' => 'required|max:255|unique:users,name',
            'user_email'=>'required|email|unique:users,email',
        ]);
        if($validator->fails())
        {
            return response()->json(['error'=>$validator->errors()],202);
        }
        $input = $request->all();
        $password = random_string('alnum',8);
        $user = User::create([
            'name' => $input['user_name'],
            'email'=> $input['user_email'],
            'password'=>Hash::make($password) 
        ]);
        $responseArray = [];
        $responseArray['token'] = $user->createToken('MyApp')->accessToken;
        $responseArray['name'] = $user->name;
        $responseArray['password'] = $password;
            
        return response()->json($responseArray,200);  
    }
    public function login(Request $request)
    {   try{
            $input= $request->json()->all();
            $email = $input["user_email"];
            $password = $input["password"];
            if(Auth::attempt(['email'=>$email, 'password'=>$password]))
            {
                $user = Auth::user();
                $responseArray = [];
                $responseArray['token'] = $user->createToken('MyApp')->accessToken;
                $responseArray['name'] = $user->name;
                
                return response(['data' => $responseArray, 'message' =>  'login success', 'status' =>true]);
            }
            else
            {
                return response()->json(['message' => 'Please check your login details','error'=>'Unauthenticated','status' =>false],203);
            }
        }catch(Exception $e){
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(), 'status' =>false ]);
        }
    }
   
    public function logout(Request $request)
     {
        // $user = Auth::guard("api")->user()->token();
        // $user->revoke();
        // $responseMessage = "successfully logged out";
        //         return response()->json([
        //         'success' => true,
        //         'message' => $responseMessage
        //         ], 200);
        if ($request->user()) { 
            $request->user()->tokens()->delete();
        }
        return response()->json(['message' => 'You are Logout'], 200);
     }
    
    
}
