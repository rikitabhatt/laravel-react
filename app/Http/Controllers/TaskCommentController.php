<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DataTables;
use DB;
use Exception;
use Validator;
use App\Models\Task_comment;
use App\Http\Resources\TaskComment\TaskCommentCollection;
class TaskCommentController extends Controller
{
    public function index(){}
    public function getAllList(Request $request, $task_id=null) 
    {   
        try {
            
             $records = Task_comment::where('task_id',$task_id)->latest()->get();
             $data =TaskCommentCollection::collection($records);
             return response(['data' => $data, 'message' => RETRIVE_RECORD,'status' => true]);
        }catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(), 'status' =>false ]);
        }
    }
    public function store(Request $request)
    {
        try {
            $input= $request->json()->all();
            $validator = Validator::make($request->json()->all(), [
                'task_id' => 'required',
                'comment'=>'required',         
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR, 'status' =>false]);
            }
            $data = Task_comment::create([
                'task_id' => $input['task_id'],
                'comment'=>$input['comment'],
                'created_by'=>$input['created_by']
            ]); 
            return response(['data' => new TaskCommentCollection($data), 'message' =>  str_replace( '{mname}','Task Comment',INSERT_SUC), 'status' =>true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' =>false], 201);
        }
    }
    public function show(Task_comment $task_comment)
    {
        return response(['data' => new TaskCommentCollection($task_comment), 'message' => RETRIVE_RECORD,'status' => true]);
    }
    public function update(Request $request, Task_comment $task_comment)
    {   try{
            $input= $request->json()->all(); 
            $validator = Validator::make($request->json()->all(), [
                'task_id' => 'required',
                'comment'=>'required',         
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR,  'status' => false]);
            }
            $update_data= array( 
                'task_id' => $input['task_id'],
                'comment'=>$input['comment'],
                'updated_by '=>isset($input['created_by'])?$input['created_by']:1
            );
            $task_comment->update($update_data);
            return response(['data' => new TaskCommentCollection($task_comment), 'message' => str_replace( '{mname}','Role',UPDATE_SUC), 'status' => true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function destroy(Task_comment $task_comment)
    {   
        try{
            $isExits  = Task_comment::find($task_comment->id);
            if($isExits){
                $task_comment->delete();
                return response(['data' => array(), 'message' => str_replace( '{mname}','Task Comment',DELETE_SUC),'status' => true], 200);
            }else {
                return response(['error' => array(), 'message'=>RECORD_NOT_FOUND,'status' => false ], 404);
            }
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
   
}
