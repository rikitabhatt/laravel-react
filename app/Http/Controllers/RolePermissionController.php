<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Role_permission;
use App\Http\Resources\RolePermission\RolePermissionCollection;
class RolePermissionController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {   
        $records = Role_permission::orderby('id','desc')->get();
        $resource = RolePermissionCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {  
        $input = $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'permission_id' => 'required|exists:permissions,id',
            'role_id'=>'required|exists:roles,id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Role_permission::create([
            'permission_id' => $input['permission_id'],
            'role_id'=>$input['role_id'],
            'created_by'=>1
        ]);  
        return response(['data' => new RolePermissionCollection($data), 'message' =>  str_replace( '{mname}','Role Permission',INSERT_SUC)], 201);
    }
    public function show(Role_permission $Role_permission)
    {
        return response(['data' => new RolePermissionCollection($Role_permission), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Role_permission $Role_permission)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'permission_id' => 'required|exists:permissions,id',
            'role_id'=>'required|exists:roles,id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'permission_id' => $input['permission_id'],
            'role_id'=>$input['role_id']
        );
        $Role_permission->update($update_data);
        return response(['data' => new RolePermissionCollection($Role_permission), 'message' => str_replace( '{mname}','Role Permission',UPDATE_SUC)], 200);
    }
    public function destroy(Role_permission $Role_permission)
    {   
        $isExits  = Role_permission::find($Role_permission->id);
        if($isExits){
            $Role_permission->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Role Permission',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Role_permission::latest()->get();
        return RolePermissionCollection::collection($data);
    }
}
