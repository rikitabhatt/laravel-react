<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Project_category;
use App\Http\Resources\ProjectCategory\ProjectCategoryCollection;
class ProjectCategoryController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {   
        $records = Project_category::orderby('id','desc')->get();
        $resource = ProjectCategoryCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'project_cat_name' => 'required|max:255|unique:project_categories,name',
            'project_cat_description'=>'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Project_category::create([
            'name' => $input['project_cat_name'],
            'description'=>$input['project_cat_description']
        ]);  
        return response(['data' => new ProjectCategoryCollection($data), 'message' =>  str_replace( '{mname}','Project Category',INSERT_SUC)], 201);
    }
    public function show(Project_category $project_category)
    {
        return response(['data' => new ProjectCategoryCollection($project_category), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Project_category $project_category)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'project_cat_name' => 'required|max:255|unique:project_categories,name,'.$project_category->id.',id',
            'project_cat_description'=>'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'name' => $input['project_cat_name'],
            'description'=>$input['project_cat_description']
        );
        $project_category->update($update_data);
        return response(['data' => new ProjectCategoryCollection($project_category), 'message' => str_replace( '{mname}','Project Category',UPDATE_SUC)], 200);
    }
    public function destroy(Project_category $project_category)
    {   
        $isExits  = Project_category::find($project_category->id);
        if($isExits){
            $project_category->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Project Category',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Project_category::latest()->get();
        return ProjectCategoryCollection::collection($data);
    }
}
