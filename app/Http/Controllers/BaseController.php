<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Http\Controllers\Controller as Controller;


class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
    	$response = [
            'success' => true,
            'result'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 200)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];
        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }
    protected function paginate($collection)
{
   
    $page = 1;
    $perPage = 20;
    if (request()->has('per_page')) {
        $perPage = (int) request()->per_page;
    }
    if (request()->has('page')) {
        $page = (int) request()->page;
    }
    /*$results['data'] = $collection->slice(($page - 1) * $perPage, $perPage)->values();
    $results['total'] = $totalRecord = count($collection);
    $results['total_page'] = ceil($totalRecord / $perPage);*/
    $results['data'] = $collection;
    $results['total'] = $totalRecord = count($collection);
    $results['total_page'] = ceil($totalRecord / $perPage);
    //print_r($results);
    return $results;

}
}