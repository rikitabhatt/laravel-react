<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Module;
use App\Http\Resources\Module\ModuleCollection;
class ModuleController extends Controller
{
    public function index()
    { }
    public function getAllList(Request $request)
    {   
        $records = Module::orderby('id','desc')->get();
        $resource =ModuleCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'module_name' => 'required|max:255|unique:modules,module_name',
            'module_label'=>'required|unique:modules,module_label',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Module::create([
            'module_name' => $input['module_name'],
            'module_label'=>$input['module_label']
            
        ]);  
        return response(['data' => new ModuleCollection($data), 'message' =>  str_replace( '{mname}','Module',INSERT_SUC)], 201);
    }
    public function show(Module $module)
    {
        return response(['data' => new ModuleCollection($module), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Module $module)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'module_name' => 'required|max:255|unique:modules,module_name,'.$module->id.',id',
            'module_label'=>'required|unique:modules,module_label,'.$module->id.',id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'module_name' => $input['module_name'],
            'module_label'=>$input['module_label']
        );
        $module->update($update_data);
        return response(['data' => new ModuleCollection($module), 'message' => str_replace( '{mname}','Module',UPDATE_SUC)], 200);
    }
    public function destroy(Module $module)
    {   
        $isExits  = Module::find($module->id);
        if($isExits){
            $module->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Module',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Module::latest()->get();
        return ModuleCollection::collection($data);
    }
}
