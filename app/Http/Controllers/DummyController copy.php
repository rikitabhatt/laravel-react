<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;
use Validator;
use App\Models\Company;
use App\Http\Resources\Company\CompanyCollection;

class DummyController extends Controller
{
   
    public function index(Request $request)
    {   
        $input= $request->json()->all();
        $data =  Company::paginate(5);
        return CompanyCollection::collection($data);
    }
   
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'company_name' => 'required|max:255|unique:company,company_name',
            'company_url'=> 'required',
            'company_email'=>'required|email|unique:company,company_email',
            'open_time'=>'required',
        ]);
        
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        
        $data = Company::create([
            'company_name' => $input['company_name'],
            'company_url' => $input['company_url'],
            'company_email'=>$input['company_email'],
            'company_phone'=>$input['company_phone'],
            'open_time' =>$input['open_time'],
            'monday'=> isset($input['monday'])?$input['monday']:'close',
            'tuesday'=> isset($input['tuesday'])?$input['tuesday']:'close',
            'wednesday'=> isset($input['wednesday'])?$input['wednesday']:'close',
            'thursday'=> isset($input['monday'])?$input['thursday']:'close',
            'friday'=> isset($input['friday'])?$input['friday']:'close',
            'saturday'=> isset($input['saturday'])?$input['saturday']:'close',
            'sunday'=> isset($input['sunday'])?$input['sunday']:'close',
        ]);
       
        return response(['data' => new CompanyCollection($data), 'message' => str_replace( '{mname}','Company',INSERT_SUC)], 201);
    }
    public function show(Company $company)
    {
        return response(['data' => new CompanyCollection($company), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Company $company)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'company_name' => 'required|max:255|unique:company,company_name,'.$company->id.',id',
            'company_url'=> 'required',
            'company_email'=>'required|email|unique:company,company_email,'.$company->id.',id',
            'open_time'=>'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $update_data= array( 
            'company_name' => $input['company_name'],
            'company_url' => $input['company_url'],
            'company_email'=>$input['company_email'],
            'company_phone'=>$input['company_phone'],
            'open_time' =>$input['open_time'],
            'monday'=> isset($input['monday'])?$input['monday']:$company->monday,
            'tuesday'=> isset($input['tuesday'])?$input['tuesday']:$company->monday,
            'wednesday'=> isset($input['wednesday'])?$input['wednesday']:$company->wednesday,
            'thursday'=> isset($input['monday'])?$input['thursday']:$company->thursday,
            'friday'=> isset($input['friday'])?$input['friday']:$company->friday,
            'saturday'=> isset($input['saturday'])?$input['saturday']:$company->saturday,
            'sunday'=> isset($input['sunday'])?$input['sunday']:$company->sunday,
        );
        $company->update($update_data);
        return response(['data' => new CompanyCollection($company), 'message' => str_replace( '{mname}','Company',UPDATE_SUC)], 200);
    }
    public function destroy(Company $company)
    {   
        $isExits  = Company::find($company->id);
        if($isExits){
            $company->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Company',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
}
