<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DataTables;
use DB;
use Exception;
use Validator;
use App\Models\Task_history;
use App\Http\Resources\TaskHistory\TaskHistoryCollection;
class TaskHistoryController extends Controller
{
    public function index(){}
    public function getAllList(Request $request, $task_id=null) 
    {   
        try {
             $records = Task_history::where('task_id',$task_id)->latest()->get();
             $data =TaskHistoryCollection::collection($records);
             return response(['data' => $data, 'message' => RETRIVE_RECORD,'status' => true]);
        }catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(), 'status' =>false ]);
        }
    }
   
    public function show(Task_history $task_comment)
    {
        return response(['data' => new TaskHistoryCollection($task_comment), 'message' => RETRIVE_RECORD,'status' => true]);
    }
   
    public function destroy(Task_history $task_comment)
    {   
        try{
            $isExits  = Task_history::find($task_comment->id);
            if($isExits){
                $task_comment->delete();
                return response(['data' => array(), 'message' => str_replace( '{mname}','Task Comment',DELETE_SUC),'status' => true], 200);
            }else {
                return response(['error' => array(), 'message'=>RECORD_NOT_FOUND,'status' => false ], 404);
            }
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
   
}
