<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;
use App\Models\Designation;

class DesignationController extends Controller
{
    public function index(){}
   
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Designation::latest()->get();
        return response(['data' => $data, 'message' =>  RETRIVE_RECORD], 201);
    }
}
