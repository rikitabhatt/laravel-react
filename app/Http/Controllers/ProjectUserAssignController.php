<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Project;
use App\Models\Project_user_assign;
use App\Http\Resources\ProjectUserAssign\ProjectUserAssignCollection;
class ProjectUserAssignController extends Controller
{
    public function index(){}
    public function getAllList(Request $request, $project_id = null)
    {   $input= $request->json()->all();
        
        $records = Project_user_assign::where('project_id',$project_id)->orderby('id','desc')->get();
        $resource = ProjectUserAssignCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'project_id' => 'required|exists:projects,id',
            'user_id'=>'required|exists:users,id',
            'designation_id'=>'required|exists:designations,id',
            //'created_by '=>'required|exists:users,id'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Project_user_assign::create([
            'project_id' => $input['project_id'],
            'user_id'=>$input['user_id'],
            'designation_id'=>$input['designation_id'],
            'created_by'=>1
        ]);  
        return response(['data' => new ProjectUserAssignCollection($data), 'message' =>  str_replace( '{mname}','Project',INSERT_SUC)], 201);
    }
    public function show(Project_user_assign $project_user_assign)
    {
        return response(['data' => new ProjectUserAssignCollection($project_user_assign), 'message' => RETRIVE_RECORD], 200);
    }
   
    public function destroy(Project_user_assign $project_user_assign)
    {   
        $isExits  = Project_user_assign::find($project_user_assign->id);
        if($isExits){
            $project_user_assign->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Remove employee from project',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        
        if(isset($input['project_id']) && !empty($input['project_id'])){
            $data = Project_user_assign::where('project_id',$input['project_id'])->get();
        }else{
            $data = Project_user_assign::latest()->get();
        }
        return ProjectUserAssignCollection::collection($data);
    }
    
}
