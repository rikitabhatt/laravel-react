<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DataTables;
use DB;
use Exception;
use Validator;
use App\Models\Module_action;
use App\Http\Resources\ModuleAction\ModuleActionCollection;
class ModuleActionController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {    try {
            $records = Module_action::orderby('id','desc')->get();
            $resource = ModuleActionCollection::collection($records);
            $result = DataTables::of($resource)->toJson();
            $data['data'] = $result;
            $data['message'] = RETRIVE_RECORD;
            $data['success'] = true;
            return $data;
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(), 'status' =>false ]);
        }
    }
    public function store(Request $request)
    {
        try {
            $input= $request->json()->all();
            $validator = Validator::make($request->json()->all(), [
                'action_name' => 'required|max:255|unique:module_actions,action_name',
                'action_label'=>'required|unique:module_actions,action_label',
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR, 'status' =>false]);
            }
            $data = Module_action::create([
                'action_name' => $input['action_name'],
                'action_label'=>$input['action_label']
            ]); 
            return response(['data' => new ModuleActionCollection($data), 'message' =>  str_replace( '{mname}','Module Action',INSERT_SUC), 'status' =>true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' =>false], 201);
        }
    }
    public function show(Module_action $module_action)
    {
        return response(['data' => new ModuleActionCollection($module_action), 'message' => RETRIVE_RECORD,'status' => true]);
    }
    public function update(Request $request, Module_action $module_action)
    {   try{
            $input= $request->json()->all(); 
            $validator = Validator::make($request->all(), [
                'action_name' => 'required|max:255|unique:module_actions,action_name,'.$module_action->id.',id',
                'action_label'=>'required|unique:module_actions,action_label,'.$module_action->id.',id',
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR,  'status' => false]);
            }
            $update_data= array( 
                'action_name' => $input['action_name'],
                'action_label'=>$input['action_label']
            );
            $module_action->update($update_data);
            return response(['data' => new ModuleActionCollection($module_action), 'message' => str_replace( '{mname}','Role',UPDATE_SUC), 'status' => true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function destroy(Module_action $module_action)
    {   
        try{
            $isExits  = Module_action::find($module_action->id);
            if($isExits){
                $module_action->delete();
                return response(['data' => array(), 'message' => str_replace( '{mname}','Module Action',DELETE_SUC),'status' => true], 200);
            }else {
                return response(['error' => array(), 'message'=>RECORD_NOT_FOUND,'status' => false ], 404);
            }
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Module_action::latest()->get();
        return ModuleActionCollection::collection($data);
    }
}
