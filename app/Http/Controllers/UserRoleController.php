<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\User_role;
use App\Http\Resources\UserRole\UserRoleCollection;
class UserRoleController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {   
        $records = User_role::orderby('id','desc')->get();
        $resource = UserRoleCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {  
        $input = $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'user_id' => 'required|exists:users,id',
            'role_id'=>'required|exists:roles,id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = User_role::create([
            'user_id' => $input['user_id'],
            'role_id'=>$input['role_id'],
            'created_by'=>1
        ]);  
        return response(['data' => new UserRoleCollection($data), 'message' =>  str_replace( '{mname}','Role Permission',INSERT_SUC)], 201);
    }
    public function show(User_role $user_role)
    {
        return response(['data' => new UserRoleCollection($user_role), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, User_role $user_role)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'role_id'=>'required|exists:roles,id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'user_id' => $input['user_id'],
            'role_id'=>$input['role_id'],
            'created_by'=>1,
        );
        $user_role->update($update_data);
        return response(['data' => new UserRoleCollection($user_role), 'message' => str_replace( '{mname}','Role Permission',UPDATE_SUC)], 200);
    }
    public function destroy(User_role $user_role)
    {   
        $isExits  = User_role::find($user_role->id);
        if($isExits){
            $user_role->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Role Permission',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = User_role::latest()->get();
        return UserRoleCollection::collection($data);
    }
}
