<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Permission;
use App\Http\Resources\Permission\PermissionCollection;
class PermissionController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {   
        $records = Permission::orderby('id','desc')->get();
        $resource = PermissionCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'module_id' => 'required|exists:modules,id',
            'module_action_id'=>'required|exists:module_actions,id',
            'title'=>'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
      
        $data = Permission::create([
            'module_id' => $input['module_id'],
            'module_action_id'=>$input['module_action_id'],
            'title'=>$input['title'],
        ]);  
        return response(['data' => new PermissionCollection($data), 'message' =>  str_replace( '{mname}','Permission',INSERT_SUC)], 201);
    }
    public function show(Permission $permission)
    {
        return response(['data' => new PermissionCollection($permission), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Permission $permission)
    {   $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'module_id' => 'required|exists:modules,id',
            'module_action_id'=>'required|exists:module_actions,id',
            'title'=>'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'module_id' => $input['module_id'],
            'module_action_id'=>$input['module_action_id'],
            'title'=>$input['title']
        );
        $permission->update($update_data);
        return response(['data' => new PermissionCollection($permission), 'message' => str_replace( '{mname}','Permission',UPDATE_SUC)], 200);
    }
    public function destroy(Permission $permission)
    {   
        $isExits  = Permission::find($permission->id);
        if($isExits){
            $permission->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Permission',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Permission::latest()->get();
        return PermissionCollection::collection($data);
    }
}
