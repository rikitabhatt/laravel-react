<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;
use DataTables;
use Validator;
use App\Models\User;
use App\Http\Resources\User\UserCollection;
class UserController extends Controller
{
    public function index()
    {
        $data =  User::paginate(5);
        return UserCollection::collection($data);
    }
    public function getAllList(Request $request)
    {   
        $records = User::orderby('id','desc')->get();
        $resource =UserCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'user_name' => 'required|max:255|unique:users,name',
            'user_email'=>'required|email|unique:users,email',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        //$password = random_string('alnum',8);
        $password = 'admin@123';
        $data = User::create([
            'name' => $input['user_name'],
            'email'=>$input['user_email'],
            'password'=>Hash::make($password) 
        ]);  
        return response(['data' => new UserCollection($data), 'message' =>  str_replace( '{mname}','User',INSERT_SUC)], 201);
    }
    public function show(User $user)
    {
        return response(['data' => new UserCollection($user), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, User $user)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'user_name' => 'required|max:255|unique:users,name,'.$user->id.',id',
            'user_email'=>'required|email|unique:users,email,'.$user->id.',id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'name' => $input['user_name'],
            'email'=>$input['user_email'],
            
        );
        $user->update($update_data);
        return response(['data' => new UserCollection($user), 'message' => str_replace( '{mname}','User',UPDATE_SUC)], 200);
    }
    public function destroy(User $user)
    {   
        $isExits  = User::find($user->id);
        if($isExits){
            $user->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','User',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        if(isset($input['role_id']) && !empty($input['role_id'])){
            $data = User::latest()->get();
        }else
        {
            $data = User::latest()->get();
        }
        return UserCollection::collection($data);
    }

}