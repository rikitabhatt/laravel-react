<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DB;
use Validator;
use App\Models\Restaurant;
use App\Http\Resources\Restaurant\RestaurantCollection;
class RestaurantController extends Controller
{
    public function index()
    {
       $data =  Restaurant::paginate(5);
       return RestaurantCollection::collection($data);
    }
    public function store(Request $request)
    {  
        $input= $request->json()->all();

   // print_r($request->all());
    $validator = Validator::make($request->json()->all(), [
            'name' => 'required|max:255|unique:restaurant,name',
            'address'=> 'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => 'Validation Error']);
        }
        
        $data = Restaurant::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'address' =>$input['address']
        ]);
       
        return response(['data' => new RestaurantCollection($data), 'message' => 'Created successfully'], 201);
    }  
    public function show(Restaurant $restaurant)
    {
        return response(['data' => new RestaurantCollection($restaurant), 'message' => 'Retrieved successfully'], 200);
    }
    public function update(Request $request, Restaurant $restaurant)
    {    
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255|unique:restaurant,name,'.$restaurant->id.',id',
            'address'=> 'required',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>'Validation Error']);
        }
        $update_data= array('name' => $request->name,
                            'address' => $request->address,
                            'email' =>$request->email,
                           );
        $restaurant->update($update_data);
    
        return response(['data' => new RestaurantCollection($restaurant), 'message' => 'Update successfully'], 200);
    }
    public function destroy(Restaurant $restaurant)
    {   
        $isExits  = Restaurant::find($restaurant->id);
        if($isExits){
        $restaurant->delete();
        return response(['data' => array(), 'message' => 'Delete successfully'], 200);
        }else {
            return response(['error' => array(), 'message'=>'Recordnot found'], 404);
        }
    }
}
