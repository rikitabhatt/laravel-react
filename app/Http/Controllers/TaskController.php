<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DataTables;
use DB;
use Exception;
use Validator;
use App\Models\Task;
use App\Models\Project;
use App\Http\Resources\Task\TaskCollection;
use Illuminate\Support\Facades\Redis;
use App\Traits\HistoryTrait;
class TaskController extends Controller
{   use HistoryTrait;
    public function index(){}
    public function getAllList(Request $request, $filterby=null) 
    {   
        try {
            if(is_null($filterby) || $filterby == 1){
                $records = Task::all();
            }else{
                $records = Task::where('id',$filterby)->get();
            }
            $data =TaskCollection::collection($records);
            return response(['data' => $data, 'message' => RETRIVE_RECORD,'status' => true]);
        }catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(), 'status' =>false ]);
        }
    }
    
    public function store(Request $request)
    {
        try {
            $input= $request->json()->all();
            $validator = Validator::make($request->json()->all(), [
                "project_id" => 'required|max:255|exists:projects,id',
                "issue_type"=> 'required',
                "issue_title"=> 'required',
                "description"=> 'required',
                "priority"=> 'required',
                "assignee_id"=> 'required|max:255|exists:users,id',
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR, 'status' =>false]);
            }
            $data = Task::create([
                "project_id" => $input['project_id'],
                "issue_type"=> $input['issue_type'],
                "issue_title"=> $input['issue_title'],
                "description"=> $input['description'],
                "priority"=> $input['priority'],
                "assignee_id"=> $input['assignee_id'],
            ]);
        
            if($data){
                $task_slug = !empty(Project::getProjectkey($data->project_id))?Project::getProjectkey(3).'-'.$data->id:$data->project_id.'-'.$data->id;
                Task::where('id', $data->id)->update(['task_slug' => $task_slug]);
            }
            $data = Task::find($data->id);
            return response(['data' => new TaskCollection($data), 'message' =>  str_replace( '{mname}','Task',INSERT_SUC), 'status' =>true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' =>false], 201);
        }
    }
    public function show(Task $task)
    {
        return response(['data' => new TaskCollection($task), 'message' => RETRIVE_RECORD,'status' => true]);
    }
    public function update(Request $request, Task $task)
    {   try{
            $input= $request->json()->all(); 
            $validator = Validator::make($request->all(), [
                "project_id" => 'required|max:255|exists:projects,id',
                "issue_type"=> 'required',
                "issue_title"=> 'required',
                "description"=> 'required',
                "priority"=> 'required',
                "assignee_id"=> 'required|max:255|exists:users,id',
                
            ]);
            if ($validator->fails()) {
                return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR,  'status' => false]);
            }
            $update_data= array( 
                "project_id" => $input['project_id'],
                "issue_type"=> $input['issue_type'],
                "issue_title"=> $input['issue_title'],
                "description"=> $input['description'],
                "priority"=> $input['priority'],
                "assignee_id"=> $input['assignee_id'],
            );
            $task->update($update_data);
            
            return response(['data' => new TaskCollection($task), 'message' => str_replace( '{mname}','Task',UPDATE_SUC), 'status' => true]);
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function destroy(Task $task)
    {   
        try{
            $isExits  = Task::find($task->id);
            if($isExits){
                $task->delete();
                return response(['data' => array(), 'message' => str_replace( '{mname}','Task',DELETE_SUC),'status' => true], 200);
            }else {
                return response(['error' => array(), 'message'=>RECORD_NOT_FOUND,'status' => false ], 404);
            }
        } catch (Exception $e) {
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Task::latest()->get();
        return TaskCollection::collection($data);
    }
    public function getAlltaskType(){
        $data = [
            ['type' =>'bug','name' =>'Bug'],
            ['type' =>'epik','name' =>'Epik'],
            ['type' =>'task','name' =>'Task']
        ];
        return response(['data' =>$data, 'message' => 'Issue type','status' => true], 200);
    }

    public function getTaskBySlug($task_slug)
    {   
        try {
            $task = Task::where('task_slug', $task_slug)->first();
            return response(['data' => new TaskCollection($task), 'message' => RETRIVE_RECORD,'status' => true]);
        }catch(Exception $e){
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function updateTaskTitle(Request $request){
        try {
            $input= $request->json()->all();
            $issue_title =  $input['issue_title'];
            $task_id = $input['task_id'];
            Task::where('id', $task_id)->update(['issue_title' => $issue_title]);
            $insertHisotry = [
                "task_id" => $task_id,
                "user_id" =>'1',
                "history_type"=>'task_title_changes' ,
                "old_value"=> $input['old_title'],
                "new_value"=> $issue_title,
            ];
            $isDone = $this->addHistory($insertHisotry);
            if(!$isDone){
                return response(['message' => 'Issue in history insert', 'error' => 'History_error','status' => false]);
            }
            $task = Task::find($task_id);
            return response(['data' => new TaskCollection($task), 'message' => RETRIVE_RECORD,'status' => true]);
        }catch(Exception $e){
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
    public function updateTaskAssignee(Request $request){
        try {
            $input= $request->json()->all();
            $assignee_id =  $input['assignee_id'];
            $task_id = $input['task_id'];
            Task::where('id', $task_id)->update(['assignee_id' => $assignee_id]);
            $insertHistory = [
                "task_id" =>$task_id,
                "user_id" =>'1',
                "history_type"=>'assignee_changes' ,
                "old_value"=> $input['old_assignee_id'],
                "new_value"=> $assignee_id,
            ];
            $isDone = $this->addHistory($insertHistory);
            if(!$isDone){
                return response(['message' => 'Issue in history insert', 'error' => 'History_error','status' => false]);
            }
            $task = Task::find($task_id);
            return response(['data' => new TaskCollection($task), 'message' => RETRIVE_RECORD,'status' => true]);
        }catch(Exception $e){
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }

    public function updateTaskStatus(Request $request){
        try {
            $input= $request->json()->all();
            $task_id = $input['task_id'];
            Task::where('id', $task_id)->update(['status' =>  $input['new_status']]);
            $insertHistory = [
                "task_id" =>$task_id,
                "user_id" =>'1',
                "history_type"=>'status_changes',
                "old_value"=> $input['old_status'],
                "new_value"=> $input['new_status'],
            ];
            $isDone = $this->addHistory($insertHistory);
            if(!$isDone){
                return response(['message' => 'Issue in history insert', 'error' => 'History_error','status' => false]);
            }
            $task = Task::find($task_id);
            return response(['data' => new TaskCollection($task), 'message' => RETRIVE_RECORD,'status' => true]);
        }catch(Exception $e){
            return response(['message' => EXCEPTION_ERROR, 'error' => $e->getMessage(),'status' => false]);
        }
    }
}
