<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Project;
use App\Http\Resources\Project\ProjectCollection;
class ProjectController extends Controller
{
    public function index(){}
    public function getAllList(Request $request)
    {   
        $records = Project::orderby('id','desc')->get();
        $resource = ProjectCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'project_name' => 'required|max:255|unique:projects,project_name',
            'project_description'=>'required',
            'project_key'=>'required',
            'project_category_id'=>'required|exists:project_categories,id'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Project::create([
            'project_name' => $input['project_name'],
            'project_description'=>$input['project_description'],
            'project_key'=>$input['project_key'],
            'project_category_id'=>$input['project_category_id']
        ]);  
        return response(['data' => new ProjectCollection($data), 'message' =>  str_replace( '{mname}','Project',INSERT_SUC)], 201);
    }
    public function show(Project $project)
    {
        return response(['data' => new ProjectCollection($project), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Project $project)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'project_name' => 'required|max:255|unique:projects,project_name,'.$project->id.',id',
            'project_description'=>'required',
            'project_key'=>'required',
            'project_category_id'=>'required|exists:project_categories,id'
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'project_name' => $input['project_name'],
            'project_description'=>$input['project_description'],
            'project_key'=>$input['project_key'],
            'project_category_id'=>$input['project_category_id']
        );
        $project->update($update_data);
        return response(['data' => new ProjectCollection($project), 'message' => str_replace( '{mname}','Project',UPDATE_SUC)], 200);
    }
    public function destroy(Project $project)
    {   
        $isExits  = Project::find($project->id);
        if($isExits){
            $project->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Project',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Project::latest()->get();
        return ProjectCollection::collection($data);
    }
   
}
