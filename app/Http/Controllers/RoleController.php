<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use DataTables;
use DB;
use Validator;
use App\Models\Role;
use App\Http\Resources\Role\RoleCollection;
class RoleController extends Controller
{
    public function index(){ }
    public function getAllList(Request $request)
    {   
        $records = Role::orderby('id','desc')->get();
        $resource =RoleCollection::collection($records);
        $result = DataTables::of($resource)->toJson();
        $data['data'] = $result;
        $data['message'] = RETRIVE_RECORD;
        return $data;
    }
    public function store(Request $request)
    {
        $input= $request->json()->all();
        $validator = Validator::make($request->json()->all(), [
            'role_name' => 'required|max:255|unique:roles,role_name',
            'role_label'=>'required|unique:roles,role_label',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' => VALIDATION_ERROR]);
        }
        $data = Role::create([
            'role_name' => $input['role_name'],
            'role_label'=>$input['role_label']
            
        ]);  
        return response(['data' => new RoleCollection($data), 'message' =>  str_replace( '{mname}','Role',INSERT_SUC)], 201);
    }
    public function show(Role $role)
    {
        return response(['data' => new RoleCollection($role), 'message' => RETRIVE_RECORD], 200);
    }
    public function update(Request $request, Role $role)
    {   
        $input= $request->json()->all(); 
        $validator = Validator::make($request->all(), [
            'role_name' => 'required|max:255|unique:roles,role_name,'.$role->id.',id',
            'role_label'=>'required|unique:roles,role_label,'.$role->id.',id',
        ]);
        if ($validator->fails()) {
            return response(['error' => $validator->errors(),  'message' =>VALIDATION_ERROR]);
        }
        $update_data= array( 
            'role_name' => $input['role_name'],
            'role_label'=>$input['role_label']
        );
        $role->update($update_data);
        return response(['data' => new RoleCollection($role), 'message' => str_replace( '{mname}','Role',UPDATE_SUC)], 200);
    }
    public function destroy(Role $role)
    {   
        $isExits  = Role::find($role->id);
        if($isExits){
            $role->delete();
            return response(['data' => array(), 'message' => str_replace( '{mname}','Role',DELETE_SUC)], 200);
        }else {
            return response(['error' => array(), 'message'=>RECORD_NOT_FOUND], 404);
        }
    }
    public function getAllRecord(Request $request){
        $input= $request->json()->all();
        $data = Role::latest()->get();
        return RoleCollection::collection($data);
    }
}
