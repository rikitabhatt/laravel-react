<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Events;
use App\Models\UserEventTrack;
use App\Models\InviteFriendsTrack;
use Carbon\Carbon;

if ( ! function_exists('sendTo')){
	function sendTo($token, $title, $message, $type, $id = null)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder('FASHHUNT');
        $notificationBuilder->setBody("Hello World")->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'notification_type' => $type,
            "title" => $title,
            "msg" => $message,
            "image" => $image,
        ]);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $fcm = FCM::sendTo($token, $option, $notification, $data);

	}
}
	
?>
