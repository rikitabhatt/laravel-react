<?php
if ( ! function_exists('trim_slashes'))
{
	/**
	 * Trim Slashes
	 *
	 * Removes any leading/trailing slashes from a string:
	 *
	 * /this/that/theother/
	 *
	 * becomes:
	 *
	 * this/that/theother
	 *
	 * @todo	Remove in version 3.1+.
	 * @deprecated	3.0.0	This is just an alias for PHP's native trim()
	 *
	 * @param	string
	 * @return	string
	 */
	function trim_slashes($str)
	{
		return trim($str, '/');
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('strip_slashes'))
{
	/**
	 * Strip Slashes
	 *
	 * Removes slashes contained in a string or in an array
	 *
	 * @param	mixed	string or array
	 * @return	mixed	string or array
	 */
	function strip_slashes($str)
	{
		if ( ! is_array($str))
		{
			return stripslashes($str);
		}

		foreach ($str as $key => $val)
		{
			$str[$key] = strip_slashes($val);
		}

		return $str;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('strip_quotes'))
{
	/**
	 * Strip Quotes
	 *
	 * Removes single and double quotes from a string
	 *
	 * @param	string
	 * @return	string
	 */
	function strip_quotes($str)
	{
		return str_replace(array('"', "'"), '', $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('quotes_to_entities'))
{
	/**
	 * Quotes to Entities
	 *
	 * Converts single and double quotes to entities
	 *
	 * @param	string
	 * @return	string
	 */
	function quotes_to_entities($str)
	{
		return str_replace(array("\'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('reduce_double_slashes'))
{
	/**
	 * Reduce Double Slashes
	 *
	 * Converts double slashes in a string to a single slash,
	 * except those found in http://
	 *
	 * http://www.some-site.com//index.php
	 *
	 * becomes:
	 *
	 * http://www.some-site.com/index.php
	 *
	 * @param	string
	 * @return	string
	 */
	function reduce_double_slashes($str)
	{
		return preg_replace('#(^|[^:])//+#', '\\1/', $str);
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('reduce_multiples'))
{
	/**
	 * Reduce Multiples
	 *
	 * Reduces multiple instances of a particular character.  Example:
	 *
	 * Fred, Bill,, Joe, Jimmy
	 *
	 * becomes:
	 *
	 * Fred, Bill, Joe, Jimmy
	 *
	 * @param	string
	 * @param	string	the character you wish to reduce
	 * @param	bool	TRUE/FALSE - whether to trim the character from the beginning/end
	 * @return	string
	 */
	function reduce_multiples($str, $character = ',', $trim = FALSE)
	{
		$str = preg_replace('#'.preg_quote($character, '#').'{2,}#', $character, $str);
		return ($trim === TRUE) ? trim($str, $character) : $str;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('random_string'))
{
	
	function random_string($type = 'alnum', $len = 8)
	{	
		switch ($type)
		{
			case 'basic':
				return mt_rand();
			case 'alnum':
			case 'numeric':
			case 'nozero':
			case 'alpha':
				switch ($type)
				{
					case 'alpha':
						$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'alnum':
						$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
						break;
					case 'numeric':
						$pool = '0123456789';
						break;
					case 'nozero':
						$pool = '123456789';
						break;
				}
				return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
			case 'unique': // todo: remove in 3.1+
			case 'md5':
				return md5(uniqid(mt_rand()));
			case 'encrypt': // todo: remove in 3.1+
			case 'sha1':
				return sha1(uniqid(mt_rand(), TRUE));
		}
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('increment_string'))
{
	/**
	 * Add's _1 to a string or increment the ending number to allow _2, _3, etc
	 *
	 * @param	string	required
	 * @param	string	What should the duplicate number be appended with
	 * @param	string	Which number should be used for the first dupe increment
	 * @return	string
	 */
	function increment_string($str, $separator = '_', $first = 1)
	{
		preg_match('/(.+)'.preg_quote($separator, '/').'([0-9]+)$/', $str, $match);
		return isset($match[2]) ? $match[1].$separator.($match[2] + 1) : $str.$separator.$first;
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('alternator'))
{
	
	function alternator()
	{
		static $i;

		if (func_num_args() === 0)
		{
			$i = 0;
			return '';
		}

		$args = func_get_args();
		return $args[($i++ % count($args))];
	}
}

// ------------------------------------------------------------------------

if ( ! function_exists('repeater'))
{
	function repeater($data, $num = 1)
	{
		return ($num > 0) ? str_repeat($data, $num) : '';
	}
}
if( ! function_exists('shortstring')){
	function shortstring($string,$limit=30,$connectoestring='...'){   
		if (strlen($string) > $limit) {
			$trimstring = substr($string, 0, $limit).$connectoestring;
			} else {
			$trimstring = $string;
			}
			return $trimstring;
	}
}
if(! function_exists('array_group')){
	function  array_group(array $data, $by_column)
	{
		$result = [];
		foreach ($data as $item) {
			$column = $item[$by_column];
			unset($item[$by_column]);
			if (isset($result[$column])) {
				$result[$column][] = $item;
			} else {
				$result[$column] = array($item);
			}
		}
		return $result;
	}
}

if( !function_exists('array_group1')){
	function array_group1($data,$key) {
		$result = array();
		foreach($data as $val) {
			if(isset($key, $val)){
				$result[$val[$key]][] = $val;
			}else{
				$result[""][] = $val;
			}
		}
		return $result;
	}
}
/*
Description : Define days sequnce for all sport.
*/
if(!function_exists('weekday')){
	function weekday(){
		$days['1'] = 'wednesday';
		$days['2'] = 'thursday';
		$days['3'] = 'friday';
		$days['4'] = 'saturday';
		$days['5'] = 'sunday';
		$days['6'] = 'monday';
		$days['7'] = 'tuesday';
		return $days;	
	}
}
/*
Description : return current week dates accoridng date. 
*/
if(!function_exists('get_week_date')){
	function get_week_date($mydate='')
	{   
		
		$mydate = empty($mydate)? date('Y-m-d'): $mydate;
		$result= array();
		$today = date('l', strtotime($mydate));
		$days = weekday();
		$current_key =  array_search($today,$days);
		$prev = array_reverse(array_slice($days, 0, $current_key-1)); 
		$next = array_slice($days, $current_key);  
		$less = $plus = 0;
		if(!empty($prev))
		{
			foreach($prev as $day)
			{
				$result[$day] = date('Y-m-d', strtotime($mydate .' -'.$less.' day'));
				$less++;
			}
		}
		if(!empty($next))
		{
			foreach($next as $day)
			{ 
				$result[$day] = date('Y-m-d', strtotime($mydate .' +'.$plus.' day'));
				$plus++;
			}
		}
		//$result[$today] = $mydate;
		$ordered_array = array_merge(array_flip($days), $result); 
		return $ordered_array;
	}
}
if(!function_exists('get_nextweek_date')){
	function get_nextweek_date($mydate='')
	{   
		
		$mydate = empty($mydate)? date('Y-m-d'): $mydate;
		$result= array();
		$today = strtolower(date('l', strtotime(date('d.m.Y',strtotime("-1 days")))));
		$days = weekday();
		$current_key =  array_search($today,$days);
		$next = array_slice($days, $current_key);  
		return $next;
		
	}
}
if(!function_exists('getWeek')){ 
function getWeek(){
	$day=strtolower(date("l"));
	$data = weekday();
	$new_date_array = array();
	$count = count($data);
	foreach($data as $key=> $dat){
		if(strpos($dat, $day) !== false){ // get the key where input day matched
				for ($i =$key; $i<=$count;$i++){ // add next all records to the new array till the end of the original array
					if(isset($data[$i])){
						$new_date_array[] = $data[$i];
					}

				}
				for ($j=0; $j<=$key-1;$j++){ // add previous one before the matched key to the new array
					if(isset($data[$j])){
						$new_date_array[] = $data[$j];
					}

				}
			}
		}return $new_date_array;
	}
	
}

   
if(!function_exists('time_ago')){ 

    function time_ago( $time)
    {
        $out    = ''; // what we will print out
        $now    = time(); // current time
        $diff   = $now - $time; // difference between the current and the provided dates

        if( $diff < 60 ) // it happened now
            return TIMEBEFORE_NOW;

        elseif( $diff < 3600 ) // it happened X minutes ago
            return str_replace( '{num}', ( $out = round( $diff / 60 ) ), $out == 1 ? TIMEBEFORE_MINUTE : TIMEBEFORE_MINUTES );

        elseif( $diff < 3600 * 24 ) // it happened X hours ago
            return str_replace( '{num}', ( $out = round( $diff / 3600 ) ), $out == 1 ? TIMEBEFORE_HOUR : TIMEBEFORE_HOURS );

        elseif( $diff < 3600 * 24 * 2 ) // it happened yesterday
            return TIMEBEFORE_YESTERDAY;

        else // falling back on a usual date format as it happened later than yesterday
            return strftime( date( 'Y', $time ) == date( 'Y' ) ? TIMEBEFORE_FORMAT : TIMEBEFORE_FORMAT_YEAR, $time );
    }
}
