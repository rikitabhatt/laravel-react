<?php
namespace App\Helpers;
class Helper {
    
        function pr($x = array()){
            echo "<pre>";print_r($x); echo "</pre>";
        }
    
   
        function prd($x = array()){
            echo "<pre>";print_r($x); echo "</pre>";exit;
        }
    
    // Encrypt Any Text
   
        function encrypt($text) {
            return base64_encode(base64_encode($text));
        }
    
    // Decrypt Any Text
    
        function decrypt($text) {
            return base64_decode(base64_decode($text));
        }
    
    //Employee type display text
   
        function employeeType($type){
            if($type == 'gm'){
                return (ucwords('General Manager'));
            }else{
                return ucwords($type);
            }
        }
   
    function getLnt($zip){
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&key=".API_KEY;
        $result_string = file_get_contents($url); 
        $result = json_decode($result_string, true);
        if(!empty($result['results']))
        {
            $result1[]=$result['results'][0];
            $result2[]=$result1[0]['geometry'];
            $result3[]=$result2[0]['location'];
            return $result3[0];
        }else {
            return array();
        }
    }
    public static  function relative_date($time) {
        $today = strtotime(date('M j, Y'));
        $reldays = ($time - $today)/86400;
        if ($reldays >= 0 && $reldays < 1) { return 'Today';} 
        else if ($reldays >= 1 && $reldays < 2) {return 'Tomorrow';} 
        else if ($reldays >= -1 && $reldays < 0) {return 'Yesterday'; }

        if (abs($reldays) < 7) {
            if ($reldays > 0) {
                $reldays = floor($reldays);
                return 'In ' . $reldays . ' day' . ($reldays != 1 ? 's' : '');
            } else {
                $reldays = abs(floor($reldays));
                return $reldays . ' day' . ($reldays != 1 ? 's' : '') . ' ago';
            }
        }
        if (abs($reldays) < 182) {
            return date('l, j F',$time ? $time : time());  
        } else {
            return date('l, j F, Y',$time ? $time : time());
        }
            
    }
    
}
?>
