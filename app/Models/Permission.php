<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Module;
use App\Models\Module_action;
class permission extends Model
{
    use HasFactory;
    protected $fillable = ['module_id','module_action_id','title','created_by'];
    public function modules(){
        return $this->belongsTo(Module::class,'module_id');
    }
    public function moduleactions(){
        return $this->belongsTo(Module_action::class,'module_action_id');
    }
}
