<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{

    use HasFactory;
    use SoftDeletes;
    protected  $table='company';
    protected $primaryKey = 'id';
    protected $fillable = [
        'company_name','company_email','company_url','open_time','company_phone','monday','tuesday','wednesday','thursday','friday','saturday','sunday'
    ];
   
}
