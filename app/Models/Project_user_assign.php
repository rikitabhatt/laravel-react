<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project_user_assign extends Model
{
    use HasFactory;
    protected $fillable = ['project_id','user_id','designation_id','created_by'];
    public function projects(){
        return $this->belongsTo(project::class,'project_id');
    }
    public function users(){
        return $this->belongsTo(user::class,'user_id');
    }
    public function designations(){
        return $this->belongsTo(designation::class,'designation_id');
    }
}
