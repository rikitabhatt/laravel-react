<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class Role extends Model
{
    use HasFactory;
    protected $fillable = [
        'role_name','role_label','created_by'
    ];
}
