<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory;
    protected $fillable = ['project_name','project_description','project_category_id','project_key']; 
    public function project_categories(){
        return $this->belongsTo(Project_category::class,'project_category_id');
    }

    public static function getProjectkey($project_id){
        $result = Project::select('project_key')->where('id',$project_id)->get();
        if(isset($result) && isset($result[0]) && !empty($result[0]))
        { return $result[0]->project_key;}else { return '';}
    }
}