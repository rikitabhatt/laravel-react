<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
use App\Models\User;
class User_role extends Model
{
    use HasFactory;
    protected $fillable = ['user_id','role_id','created_by'];
    public function roles(){
        return $this->belongsTo(Role::class,'role_id');
    }
    public function users() {
        return $this->belongsToMany('App\Models\User','user_roles','role_id','user_id');
    }
}
