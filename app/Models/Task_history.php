<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task_history extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['task_id','user_id','history_type','old_value','new_value'];
    public function tasks() {
        return $this->belongsTo('App\Models\task','task_id');
    }
    public function users(){
        return $this->belongsTo(User::class,'user_id');
    }
}