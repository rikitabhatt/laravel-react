<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task_comment extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = ['task_id','comment','created_by','update_by'];
    public function tasks() {
        return $this->belongsTo('App\Models\task','task_id');
    }
    public function users(){
        return $this->belongsTo(User::class,'created_by');
    }
}