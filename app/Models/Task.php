<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory;
    use SoftDeletes;
   // protected  $table='tasks';
    //protected $primaryKey = 'id';
    const VERY_HIGH = 1;
    const HIGH = 2;
    const MEDIUM = 3;
    const LOW = 4;
   
    protected $fillable = [
        'project_id','reporter_id','creator_id','issue_type','issue_title','description','priority','is_done','status',
        'due_date','completed_date','original_estimate','estimate','spent','assignee_id'
    ];
    public function projects(){
        return $this->belongsTo(Project::class,'project_id');
    }
    public function reporters(){
        return $this->belongsTo(User::class,'reporter_id');
    }
    public function assignees(){
        return $this->belongsTo(User::class,'assignee_id');
    }
    public function creators(){
        return $this->belongsTo(User::class,'creator_id');
    }
    public function comments() {
        return $this->hasMany('App\Models\Task_comment','task_id');
    }
    public static function getPriorityName ($priority){
        if($priority == self::VERY_HIGH){
            return 'Very High';
        }elseif($priority == self::HIGH){
            return 'High';
        }elseif($priority == self::MEDIUM){
            return 'Medium';
        }elseif($priority == self::LOW){
            return 'Low';
        }
    }
}