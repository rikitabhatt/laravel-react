<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Module_action extends Model
{
    use HasFactory;
    protected $fillable = [
        'action_name','action_label','created_by'
    ];
}
