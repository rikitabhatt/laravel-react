<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Roles;
use App\Models\Permission;
class Role_permission extends Model
{
    use HasFactory;
    protected $fillable = ['permission_id','role_id','created_by'];
    public function roles(){
        return $this->belongsTo(Role::class,'role_id');
    }
    public function permissions(){
        return $this->belongsTo(permission::class,'permission_id');
    }
}
