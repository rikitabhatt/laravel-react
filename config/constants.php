<?php
/*------ Record insert/ delete /update/ status change *------------ */
defined('INSERT_SUC')        OR define('INSERT_SUC','{mname} added successfully');
defined('UPDATE_SUC')        OR define('UPDATE_SUC', '{mname} update Sucessfully');
defined('DELETE_SUC')        OR define('DELETE_SUC', '{mname} has been Remove Sucessfully');
defined('ACTIVE_SUC')        OR define('ACTIVE_SUC', '{mname} has been Activated Sucessfully');
defined('INACTIVE_SUC')      OR define('INACTIVE_SUC', '{mname} has been In-activated Sucessfully');
defined('MULDELETE_SUC')     OR define('MULDELETE_SUC', '{mname} has been Remove Sucessfully');
defined('MULACTIVE_SUC')     OR define('MULACTIVE_SUC', '{mname}\'s has been Activated Sucessfully');
defined('MULINACTIVE_SUC')   OR define('MULINACTIVE_SUC', '{mname}\'s has been In-activated Sucessfully');
defined('ACCEPT_SUC')        OR define('ACCEPT_SUC', '{mname} has been Aceepted Sucessfully');
defined('REJECT_SUC')        OR define('REJECT_SUC', '{mname} has been Rejected Sucessfully');
defined('RECORD_NOT_FOUND')  OR define('RECORD_NOT_FOUND','Record not found');
defined('RETRIVE_RECORD')  OR define('RETRIVE_RECORD','Retrieved successfully');
defined('VALIDATION_ERROR') OR define('VALIDATION_ERROR','Validation Error');
defined('EXCEPTION_ERROR') OR define('EXCEPTION_ERROR','Exception Error');
/*---- Site Configuration -----*/
defined('ADMIN_EMAIL')       OR define('ADMIN_EMAIL', 'admin@yopmail.com');
defined('ADMIN_EMAIL_NAME')  OR define('ADMIN_EMAIL_NAME', 'Project Management');
defined('SITE_NAME') OR define('SITE_NAME', 'Project Management');
defined('API_KEY') OR define('API_KEY','');
/*
%m/%d/%Y	06/05/2013
%A, %B %e, %Y	Sunday, June 5, 2013
%b %e %a	Jun 5 Sun
Time
%H:%M	23:05
%I:%M %p	11:05 PM
*/
defined( 'TIMEBEFORE_NOW') OR define('TIMEBEFORE_NOW', 'now');
defined( 'TIMEBEFORE_MINUTE') OR define('TIMEBEFORE_MINUTE', '{num} minute ago');
defined( 'TIMEBEFORE_MINUTES') OR define('TIMEBEFORE_MINUTES', '{num} minutes ago');
defined( 'TIMEBEFORE_HOUR') OR define('TIMEBEFORE_HOUR', '{num} hour ago');
defined( 'TIMEBEFORE_HOURS') OR define('TIMEBEFORE_HOURS', '{num} hours ago');
defined( 'TIMEBEFORE_YESTERDAY')  OR define('TIMEBEFORE_YESTERDAY', 'yesterday');
defined( 'TIMEBEFORE_FORMAT_YEAR') OR define('TIMEBEFORE_FORMAT_YEAR', '');
defined( 'TIMEBEFORE_FORMAT')  OR define('TIMEBEFORE_FORMAT', '%b %e, %Y %H:%M');
defined( 'TIMEBEFORE_FORMAT_YEAR') OR define('TIMEBEFORE_FORMAT_YEAR', '%e %b, %Y');